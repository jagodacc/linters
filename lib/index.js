module.exports = {
    root: true,
    reportUnusedDisableDirectives: true,
    extends: [
        'eslint:recommended',
        'plugin:node/recommended',
        'plugin:react/recommended',
        'plugin:react-hooks/recommended'
    ],
    parserOptions: {
        requireConfigFile: false,
        sourceType: 'module',
        allowImportExportEverywhere: true,
        codeFrame: false,
        ecmaVersion: 2019,
        ecmaFeatures: {
            jsx: true
        }
    },
    env: {
        browser: true,
        es6: true,
        node: true
    },
    plugins: [
        'import',
        'node',
        'react',
        'react-hooks'
    ],
    rules: {
        'array-bracket-newline': ['error', 'consistent'],
        'array-element-newline': ['error', 'consistent'],
        'arrow-spacing': 'error',
        'brace-style': 'error',
        'comma-dangle': ['error', 'never'],
        'comma-spacing': ['error', { before: false, after: true }],
        'curly': 'error',
        'eol-last': ['error', 'always'],
        'eqeqeq': ['error', 'always'],
        'func-call-spacing': ['error', 'never'],
        'generator-star-spacing': ['error', { before: false, after: true }],
        'global-require': 'error',
        'indent': ['error', 4, { CallExpression: { arguments: 'off' }, flatTernaryExpressions: false, SwitchCase: 1, VariableDeclarator: 1 }],
        'jsx-quotes': ['error', 'prefer-double'],
        'key-spacing': ['error', { beforeColon: false, afterColon: true }],
        'keyword-spacing': ['error', { before: true }],
        'lines-between-class-members': ['error', 'always'],
        'linebreak-style': ['error', 'unix'],
        'max-classes-per-file': ['warn', { ignoreExpressions: true, max: 1 }],
        'max-len': ['error', { code: 180, comments: 180, ignoreRegExpLiterals: true, ignoreTemplateLiterals: true, ignoreUrls: true }],
        'max-statements-per-line': ['error', { max: 1 }],
        'multiline-ternary': ['error', 'always-multiline'],
        'newline-before-return': 'error',
        'no-alert': 'error',
        'no-buffer-constructor': 'error',
        'no-console': ['error', { allow: ['warn', 'error', 'info', 'debug'] }],
        'no-duplicate-imports': 'error',
        'no-empty': 'error',
        'no-empty-function': 'error',
        'no-extra-semi': 'error',
        'no-implicit-coercion': ['error', { boolean: true, disallowTemplateShorthand: true, number: true, string: true }],
        'no-lonely-if': 'error',
        'no-loop-func': 'error',
        'no-magic-numbers': 'off',
        'no-multiple-empty-lines': ['error', { max: 1, maxBOF: 0, maxEOF: 0 }],
        'no-multi-spaces': 'error',
        'no-process-exit': 'off',
        'no-trailing-spaces': 'error',
        'no-unused-vars': 'error',
        'no-var': 'error',
        'object-curly-spacing': ['error', 'always'],
        'object-shorthand': ['error', 'properties'],
        'operator-linebreak': ['error', 'before'],
        'padded-blocks': ['error', { blocks: 'never', classes: 'never', switches: 'never' }, { allowSingleLineBlocks: false }],
        'padding-line-between-statements': [
            'error',
            { blankLine: 'always', prev: ['block', 'block-like', 'class', 'directive'], next: '*' },
            { blankLine: 'always', prev: '*', next: ['block', 'block-like', 'class'] },
            { blankLine: 'never', prev: 'case', next: ['case', 'default'] }
        ],
        'prefer-arrow-callback': 'error',
        'prefer-const': 'error',
        'prefer-promise-reject-errors': 'off',
        'prefer-template': 'error',
        'quote-props': ['error', 'consistent-as-needed'],
        'quotes': ['error', 'single'],
        'require-atomic-updates': 'error',
        'require-await': 'error',
        'semi': ['error', 'always'],
        'semi-spacing': ['error', { after: true, before: false }],
        'space-before-blocks': 'error',
        'space-before-function-paren': ['error', { anonymous: 'never', asyncArrow: 'always', named: 'never' }],
        'space-infix-ops': 'error',
        'space-in-parens': ['error', 'never'],
        'yoda': 'error',

        'import/newline-after-import': ['error', { count: 1 }],
        'import/no-extraneous-dependencies': 'error',

        'node/no-unpublished-import': 'off',
        'node/no-unpublished-require': 'off',

        'react/jsx-boolean-value': ['error', 'always'],
        'react/jsx-closing-bracket-location': ['error', 'line-aligned'],
        'react/jsx-curly-spacing': ['error', 'never', { allowMultiline: false }],
        'react/jsx-max-props-per-line': ['error', { maximum: 3 }],
        'react/self-closing-comp': 'error',
        'react/sort-comp': 'error'
    },
    overrides: [
        {
            files: ['*.ts', '*.tsx'],
            parser: '@typescript-eslint/parser',
            extends: [
                'plugin:@typescript-eslint/recommended'
            ],
            plugins: [
                '@typescript-eslint'
            ],
            rules: {
                '@typescript-eslint/explicit-function-return-type': 'warn',
                '@typescript-eslint/explicit-member-accessibility': ['warn', { overrides: { constructors: 'no-public' } }],
                '@typescript-eslint/member-delimiter-style': [
                    'error',
                    {
                        multiline: { delimiter: 'semi', requireLast: true },
                        singleline: { delimiter: 'semi', requireLast: true },
                        multilineDetection: 'brackets'
                    }
                ],
                '@typescript-eslint/prefer-optional-chain': 'error',
                '@typescript-eslint/prefer-ts-expect-error': 'error',
                '@typescript-eslint/semi': ['error', 'always'],
                '@typescript-eslint/space-infix-ops': ['error', { int32Hint: false }],
                '@typescript-eslint/type-annotation-spacing': ['error', { before: false, after: true, overrides: { arrow: { before: true, after: true } } }],
                'node/no-missing-import': ['error', { tryExtensions: ['.js', '.json', '.jsx', '.ts', '.tsx'] }],
                'node/no-unsupported-features/es-builtins': 'off',
                'node/no-unsupported-features/es-syntax': 'off',
                'semi': 'off',
                'space-infix-ops': 'off'
            }
        }
    ],
    settings: {
        react: {
            version: 'detect'
        }
    }
};
